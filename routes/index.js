var express = require('express');
var router = express.Router();
var home = require('../data/home.json');
var resume = require('../data/resume.json');
var skills = require('../data/skills.json');
var portfolio = require('../data/portfolio.json');
var blog = require('../data/blog.json');
var contact = require('../data/contact.json');
var testimonials = require('../data/testimonials.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    home: home,
  });
});

router.get('/a-propos-moi', (req, res) => {
  res.render('home', {
    page: 'me',
    home: home,
    testimonials: testimonials
  });
});

router.get('/resume', (req, res) => {
  res.render('resume', {
    page: 'resume',
    resume: resume,
    skills: skills
  });
});

router.get('/portfolio', (req, res) => {
  res.render('portfolio', {
    page: 'portfolio',
    portfolio: portfolio,
  //  skills: skills
  });
});

router.get('/contact', (req, res) => {
  res.render('contact', {
    page: 'contact',
    home: home,
    contact: contact
  //  skills: skills
  });
});

router.get('/blog', (req, res) => {
  res.render('blog', {
    page: 'blog'
  });
});

router.get('/portfolio/:id', (req, res) => {
  let id = req.params.id;
  console.log(id);
  //console.log(portfolio.projects);
  const projet = portfolio.projects.filter(x => x.id == id)[0];
  res.render('portfolio_s', {
    project: projet
  });
});

router.get('**', (req, res) => {
  res.render('404');
});

module.exports = router;
